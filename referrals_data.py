import pandas as pd, numpy as np


def load_data():
    hunt = pd.read_csv("C:\\Users\\Nicolo.Visona\\tandem\\data\\tandem.raven.fraudreferrals\\Hunter rule matches.csv",
                     encoding='latin1')
    rules = pd.read_excel("C:\\Users\\Nicolo.Visona\\tandem\\data\\tandem.raven.fraudreferrals\\removed_hunter_rules.xlsx",
                     encoding='latin1')
    hunt.columns = [s.replace(' ','') for s in list(hunt.columns)]
    hunt['MatchDate'] = pd.to_datetime(hunt['MatchDate'], dayfirst=True)
    hunt.drop('WorkStatus', axis=1, inplace=True)

    return hunt, rules


def accounts_only_removed_rules(hunt, rules):

    set_rules = set(rules.Rule.values)
    g = hunt.groupby(by=['IdentifierLHS', 'Status', 'WorkStatus', 'MatchDate'])['RuleName'].unique().reset_index()

    def only_removed_rules(row):
        s = set(row['RuleName'])
        if len(s.difference(set_rules)) == 0:
            return 1
        else:
            return 0

    g['only_removed_rules'] = g.apply(only_removed_rules, axis=1)

    accs = list(g.loc[g.only_removed_rules==1, 'IdentifierLHS'])

    tmp = g[g.IdentifierLHS.isin(accs)]

    g = tmp[['IdentifierLHS', 'only_removed_rules']].groupby(by='IdentifierLHS')['only_removed_rules'].nunique().reset_index()
    g = g[g.only_removed_rules==1]

    return list(g.IdentifierLHS.values)


def df_prep(hunt):

    hunt.loc[hunt.RuleName.isin(['DMC_DETECT_RULE_CODE_', 'DJP_DETECT_RULE_CODE_', 'DMP_DETECT_RULE_CODE_']), 'RuleName'] = 'DETECT_RULE_CODE_'

    dum = pd.get_dummies(hunt['RuleName'])
    dum = pd.concat([hunt[['IdentifierLHS', 'Status', 'MatchDate']], dum], axis=1)
    dum = dum.groupby(by=['IdentifierLHS', 'Status']).sum().reset_index()
    non_un_acc = ['CC00000002096099', 'CC00000003019150']
    dum = dum[~dum.IdentifierLHS.isin(non_un_acc)]
    dum.Status = np.where(dum.Status == 'Clear', 0, 1)

    cols = [c for c in dum.columns if c not in ['IdentifierLHS', 'Status']]
    for c in cols:
        dum.loc[dum[c]!=0, c] = 1

hunt, rules = load_data()
acc_to_remove = accounts_only_removed_rules(hunt, rules)
hunt = hunt[~hunt.IdentifierLHS.isin(acc_to_remove)]





