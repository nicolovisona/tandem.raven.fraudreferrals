import pandas as pd
from .accounts_with_removed_rules import accounts_only_removed_rules

hunt_rules_path = "C:\\Users\\Nicolo.Visona\\tandem\\data\\tandem.raven.fraudreferrals\\Hunter rule matches.csv"
hunt = pd.read_csv(hunt_rules_path, encoding='latin1')

new_cols = [s.replace(' ','') for s in list(hunt.columns)]
hunt.columns = new_cols

if hunt.WorkStatus.nunique()==1:
    hunt.drop('WorkStatus', axis=1, inplace=True)

accounts_now_good = accounts_only_removed_rules()

hunt = hunt[~hunt.IdentifierLHS.isin(accounts_now_good)]

hunt['MatchDate'] = pd.to_datetime(hunt['MatchDate'], dayfirst=True)
hunt['RuleOrigin'] = hunt['RuleName'].apply(lambda x: x.split('_')[0])


